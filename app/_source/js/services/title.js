app.factory('Title', function(){
  var title = 'AppTitle';
  return {
    show: function() { return title; },
    set: function(newTitle) { title = newTitle; }
  };
});