describe('Sessão Testada >> ', function () {

    var $rootScope,
        $scope,
        controller; 

    beforeEach(function(){
        module('App');
        inject(function( $injector ){
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            controller = $injector.get('$controller')("AppCtrl", {$scope : $scope});
        });
    });
  
    
    describe("Area Testada >> ", function() {
        it("should have a message of $scope.message", function() {
           expect( $scope.message ).toBe("nvieira");
        });
    });

 
});