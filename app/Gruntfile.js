module.exports = function(grunt) {
    grunt.initConfig({
          
           pkg: grunt.file.readJSON('package.json'),



            // Define our source and build folders
            base_path: '',
 
            css_build: '_public/css',
            js_build:  '_public/js',
            vendor:    '_public/vendors', 
  
            css_src:   '_source/css',
            js_src:    '_source/js',

            test:      'test',



            meta: {
              css : {
                  banner:
                  '/*!\n' +
                  ' * app.css <%= pkg.name %> (<%= grunt.template.today("yyyy-mm-dd, HH:MM") %>)\n' +
                  ' * Author <%= pkg.author %>\n' +
                  ' */'
              },

              js : {

                  banner:
                  '/*!\n' +
                  ' * app.js <%= pkg.name %> (<%= grunt.template.today("yyyy-mm-dd, HH:MM") %>)\n' +
                  ' * Author <%= pkg.author %>\n' +
                  ' */'

              }
              
            },


            // Less Config
            less: {
              '<%= css_build %>/app.css': '<%= css_src %>/application.less'
            },
 
 
            watch: {   

                css: {
                  files: ['<%= css_src %>/*.less','<%= css_src %>/**/*'], 
                  tasks: ['css'],
                  options: { livereload: true },
                }, 

                js: {
                  files: ['<%= js_src %>/*.js', '<%= js_src %>/**/*.js'],
                  tasks: ['js'],
                  options: { livereload: true },
                }, 
 
                html: {
                  files: ['_source/*.jade','_source/views/*.jade', '_source/views/**/*.jade'], 
                  tasks: ['jade'],
                  options: { livereload: true },
                },  
              
            }, 


            jshint: { 

              ignore_warning: {
                options: {
                  '-W033': true,
                  '-W099': true,
                },
                src: ['<%= js_src %>/*.js', '<%= js_src %>/**/*.js', '<%= test %>/*.js', '<%= test %>/**/*.js'],
              },

            },
  
            concat: {
                options:{
                  separator: ';'
                },
                basic_and_extras: {
                  files: {
                     '<%= js_build %>/app.js' : ['<%= js_src %>/app.js', '<%= js_src %>/filter/*','<%= js_src %>/config/*','<%= js_src %>/services/*','<%= js_src %>/controller/*','<%= js_src %>/directive/*', '<%= vendor %>/angular-loading-bar/src/loading-bar.js']
                  }, 
                },  
            },



            uglify: {
                options: {
                  banner: '<%= meta.js.banner %>\n',
                  mangle: false
                },
                 build: {
                  files: {
                     '<%= js_build %>/app.min.js': ['<%= js_build %>/app.js'],
                  }, 
                }, 
              }, 

            
            // CSSmin Config 
            cssmin: {
                options: {
                    banner: '<%= meta.css.banner %>\n',
                    keepSpecialComments: 0
                },
                compress: {
                  files: { 
                    '<%= css_build %>/app.min.css': [ '<%= css_build %>/app.css' ],
                  }
                }
            },

            cmq: {
              GroupMediaQuerys: {
                 files: {
                  '<%= css_build %>': ['<%= css_build %>/*.css']
                }
              }
            },


            jade: {
                compile: {
                    options: {
                        client: false,
                        pretty: true
                    },
                    files: [ {
                      cwd: "_source",
                      src: "**/*.jade",
                      dest: "_public",
                      expand: true,
                      ext: ".html"
                    } ]

                }
            },

            karma: {
              unit: {
                configFile: 'karma.conf.js',
                autoWatch: true
              }
            }

    });

    // carrega plugins
    grunt.loadNpmTasks('grunt-contrib-less');  
  
    grunt.loadNpmTasks( 'grunt-contrib-jshint' );
    grunt.loadNpmTasks( 'grunt-contrib-concat' );
    grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-combine-media-queries' );
    grunt.loadNpmTasks( 'grunt-contrib-jade' );

    grunt.loadNpmTasks('grunt-contrib-watch');  
    grunt.loadNpmTasks('grunt-karma');

  

  
    grunt.registerTask('css', ['less','cmq','cssmin']);
    grunt.registerTask('js', ['jshint', 'concat', 'uglify']); 


    grunt.registerTask('test', ['karma']); 
 

    grunt.registerTask('default', ['less', 'cmq', 'jshint', 'concat', 'cssmin', 'uglify']);
 

};